# Lighthouse

A container image to run the [Google Chrome Lighthouse tool](https://developer.chrome.com/docs/lighthouse/overview/)
via the CLI to analyze website performance, best practices, etc.

This image is based on the Node 18 Debian Bullseye Slim image and includes
the following dependencies:

- `chromium`: The latest Chromium release for Debian Bullseye.
- [`@lhci/cli`](https://www.npmjs.com/package/@lhci/cli): The npm package for
  running Lighthouse CI via the CLI. See the documentation for details on all
  CLI arguments and configuration options.
- [`lighthouse`](https://www.npmjs.com/package/lighthouse): The npm package for
  running Lighthouse via the CLI. See the documentation for details on all CLI
  arguments and configuration options.
- [`serve`](https://www.npmjs.com/package/serve): A CLI-based static web server.
- [`wait-on`](https://www.npmjs.com/package/wait-on): A CLI-based tool to wait
  for a web server (or other resources) to become available.

Note: this image runs by default as the non-privileged user `lhuser`, since
running as root requires running chromium without a sandbox. If you are trying
to run without `--no-sandbox` and are getting a `No usable sandbox!` error,
see the discussion [here](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci#the-dreaded-no-usable-sandbox-error).
If running on gitlab.com with their shared Linux runners, the seccomp policy
should allow you to run without the `--no-sandbox` flag.

## GitLab CI usage

The following shows an example GitLab CI job using this image to run
Lighthouse against a local static server.

```yaml
lighthouse:
  image: registry.gitlab.com/gitlab-ci-utils/lighthouse:latest
  stage: test
  script:
    # Start a local static server, serving the site in the ./site/ folder,
    # piping the output to null so it is not displayed, and waiting
    # until the server is available (port 3000 is the default for serve).
    - serve ./site/ > /dev/null 2>&1 & wait-on http://localhost:3000/
    # Run lighthouse against the test site in headless mode, but otherwise
    # with the default settings.
    - lighthouse http://localhost:3000 --chrome-flags="--headless"
  artifacts:
    # Always save artifacts. This is needed if lighthouse is run configured
    # to fail on certain criteria, and will ensure the report is saved.
    when: always
    # Save the lighthouse report, which by default is named for the site
    # analyzed and the current time.
    paths:
      - localhost*.html
```

Lighthouse CI provides a wrapper around Lighthouse to analyze multiple
pages, track changes over time, etc. See the
[Lighthouse CI](https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/getting-started.md)
documentation for command line and configuration options.

## Lighthouse container images

All available container image tags are available in the `gitlab-ci-utils/lighthouse`
repository at in this project's
[container registry](https://gitlab.com/gitlab-ci-utils/lighthouse/container_registry).
Details on each release is available on the
[Releases](https://gitlab.com/gitlab-ci-utils/lighthouse/releases) page.

The following tag conventions are used:

- `latest`: The latest Lighthouse image, rebuilt at least weekly, or when
  dependencies are updated.
- `x.y.z`: The image last built with a specific release of this project, as
  detailed on the [Releases](https://gitlab.com/gitlab-ci-utils/lighthouse/releases)
  page. Note these images are **not** rebuilt.

**Note:**

- Any other images in the `gitlab-ci-utils/lighthouse` repository are from
  feature branches used during development and may be deleted at any point.
- Any images in the `gitlab-ci-utils/lighthouse/tmp` repository are temporary
  images used during the build process and may be deleted at any point.
