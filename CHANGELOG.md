# Changelog

## v7.6.0 (2025-03-08)

### Changed

- Updated to Chromium 134.

### Fixed

- Refreshed lockfile to resolve `axios` vulnerability
  [CVE-2025-27152](https://osv.dev/vulnerability/GHSA-jr5f-v2jv-69x6).

## v7.5.0 (2025-02-26)

### Changed

- Updated to [`lighthouse@12.4.0`](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.4.0).

### Fixed

- Updated base image to latest Node.js v22.14.0 security update.

## v7.4.2 (2025-02-22)

### Fixed

- Updated to latest Chromium 133 security patches.

## v7.4.1 (2025-02-14)

### Fixed

- Updated to latest Chromium 133 security patches.
- Updated base image to Node.js v22.14.0.

## v7.4.0 (2025-02-08)

### Changed

- Updated to Chromium 133.

### Fixed

- Updated base image to Node.js v22.13.1.

## v7.3.2 (2025-02-02)

### Fixed

- Updated to latest Chromium 132 security patches.

## v7.3.1 (2025-01-25)

### Fixed

- Updated to latest Chromium 132 security patches.
- Updated base image to Node.js v22.13.1.

## v7.3.0 (2025-01-16)

### Changed

- Updated to Chromium 132.

### Fixed

- Updated base image to the latest Node.js v22.13.0 with security patches.

## v7.2.3 (2025-01-12)

### Fixed

- Updated to latest Chromium 131 security patch.
- Updated to [`wait-on@8.0.2`](https://github.com/jeffbski/wait-on/releases/tag/v8.0.2).
- Updated base image to Node.js v22.13.0.

## v7.2.2 (2024-12-26)

### Fixed

- Updated base image to the latest Node.js v22.12.0 with security patches.

## v7.2.1 (2024-12-20)

### Fixed

- Updated to latest Chromium 131 security patch.

## v7.2.0 (2024-12-13)

### Changed

- Updated to [`lighthouse@12.3.0`](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.3.0).

### Fixed

- Updated to latest Chromium 131 security patch.

## v7.1.1 (2024-12-08)

### Fixed

- Refreshed lockfile to resolve CVE-2024-52798.
- Updated base image to Node.js v22.12.0.
- Updated to latest Chromium 131 security patch.

## v7.1.0 (2024-11-23)

### Changed

- Updated to Chromium 131.

### Fixed

- Refreshed lockfile to resolve
  [CVE-2024-21538](https://nvd.nist.gov/vuln/detail/CVE-2024-21538) /
  [GHSA-3xgq-45jj-v275](https://osv.dev/vulnerability/GHSA-3xgq-45jj-v275).

## v7.0.0 (2024-11-14)

### Changed

- BREAKING: Updated base image to Node.js v22.11.0, which is the Active LTS
  release as of 2024-10-29. (#15)

### Fixed

- Updated to [`lighthouse@12.2.2`](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.2.2).
- Updated to latest Chromium 130 security patch.

### Miscellaneous

- Update `container_scanning` report and SBOM to reference the final built
  image instead of temporary image, which is what is scanned.

## v6.4.0 (2024-10-20)

### Changed

- Updated to Chromium 130.

## v6.3.2 (2024-10-17)

### Fixed

- Updated to `serve@14.2.4`, resolving CVE-2024-45296.
- Updated to latest Chromium 129 security patch.
- Updated base image to Node.js v20.18.0.

## v6.3.1 (2024-09-28)

### Fixed

- Updated to latest Chromium 129 security patch.
- Updated CI pipeline to update image `annotations` to be correct for this
  project. Previously, the `annotations` from the base `node` image
  were cascading to this image.

### Miscellaenous

- Updated CI pipeline to test both mobile and desktop configurations.

## v6.3.0 (2024-09-19)

### Changed

- Updated to Chromium 129.

## v6.2.2 (2024-09-14)

### Fixed

- Refreshed lockfile to resolve multiple vulnerabilities in `@lhci/cli`, specifically
  [CVE-2024-45590](https://osv.dev/vulnerability/GHSA-qwcr-r2fm-qrc7),
  [CVE-2024-43796](https://osv.dev/vulnerability/GHSA-qw6h-vgh9-j6wx),
  [CVE-2024-43799](https://osv.dev/vulnerability/GHSA-m6fv-jmcg-4jfg),
  and [CVE-2024-43800](https://osv.dev/vulnerability/GHSA-cm22-4g7w-348p).
- Updated to [`wait-on@8.0.1`](https://github.com/jeffbski/wait-on/releases/tag/v8.0.1).

## v6.2.1 (2024-09-06)

### Fixed

- Updated to [`lighthouse@12.2.1`](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.2.1).

## v6.2.0 (2024-08-23)

### Changed

- Updated to Chromium 128.

### Fixed

- Updated base image to Node.js v20.17.0.

## v6.1.2 (2024-08-20)

### Fixed

- Updated to [`wait-on@8.0.0`](https://github.com/jeffbski/wait-on/releases/tag/v8.0.0).
  This change may be BREAKING if used to wait for a Unix socket connection,
  which is not expected for this project's use, so it was considered a patch
  release.

## v6.1.1 (2024-08-15)

### Fixed

- Refreshed lockfile to resolve
  [CVE-2024-39338](https://nvd.nist.gov/vuln/detail/CVE-2024-39338) /
  [GHSA-8hc4-vh64-cxmj](https://osv.dev/vulnerability/GHSA-8hc4-vh64-cxmj).

## v6.1.0 (2024-08-07)

### Changed

- Updated to [`lighthouse@12.2.0`](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.2.0).
- Updated to Chromium 127.

## v6.0.2 (2024-07-25)

### Fixed

- Updated base image to Node.js v20.16.0.

## v6.0.1 (2024-06-28)

### Fixed

- Update `ws` package to resolve [CVE-2024-37890](https://osv.dev/vulnerability/GHSA-3h5v-q93c-6h6q).
- Updated base image to Node.js v20.15.0.

## v6.0.0 (2024-06-21)

### Changed

- BREAKING: Updated to [`@lhci/cli@0.14.0`](https://github.com/GoogleChrome/lighthouse-ci/releases),
  which updates to `lighthouse@12.0.0` (see the
  [breaking changes](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.0.0)).

## v5.3.0 (2024-06-18)

### Changed

- Updated to [`lighthouse@12.1.0`](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.1.0).

## v5.2.0 (2024-06-15)

### Changed

- Updated to Chromium 126.

## v5.1.0 (2024-05-17)

### Changed

- Updated to Chromium 125.

## v5.0.1 (2024-04-25)

### Changed

- Updated to `serve@14.2.3`

## v5.0.0 (2024-04-22)

### Changed

- BREAKING: Updated to `lighthouse@12.0.0`. See the
  [breaking changes](https://github.com/GoogleChrome/lighthouse/releases/tag/v12.0.0)
  in v12.0.0.
- Updated to `serve@14.2.2`

## v4.9.0 (2024-04-20)

### Changed

- Updated to Chromium 124.

## v4.8.2 (2024-04-13)

### Fixed

- Updated to Node 20.12.2, resolving [CVE-2024-27980](https://nodejs.org/en/blog/vulnerability/april-2024-security-releases-2#command-injection-via-args-parameter-of-child_processspawn-without-shell-option-enabled-on-windows-cve-2024-27980---high).

## v4.8.1 (2024-04-08)

### Changed

- Updated to [`lighthouse@11.7.1`](https://github.com/GoogleChrome/lighthouse/releases/tag/v11.7.1).

## v4.8.0 (2024-03-29)

### Changed

- Updated to Chromium 123.

### Fixed

- Refreshed lockfile to resolve CVE-2024-29041.

## v4.7.0 (2024-03-20)

### Changed

- Updated to [`lighthouse@11.7.0`](https://github.com/GoogleChrome/lighthouse/releases/tag/v11.7.0).

## v4.6.2 (2024-03-17)

### Fixed

- Update all dependencies, resolving [CVE-2024-28849](https://osv.dev/vulnerability/GHSA-cxjh-pqwp-8mfp).

## v4.6.1 (2024-03-05)

### Fixed

- Update all dependencies, resolving [CVE-2023-42282](https://osv.dev/vulnerability/GHSA-78xj-cgh5-2h22).

## v4.6.0 (2024-02-23)

### Changed

- Updated to Chromium 122.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#13)

## v4.5.0 (2024-02-21)

### Changed

- Updated to `lighthouse@11.6.0`.

## v4.4.0 (2024-01-24)

### Changed

- Updated to `lighthouse@11.5.0`.
- Updated to Chromium 121.

## v4.3.0 (2023-12-15)

### Changed

- Updated to `@lhci/cli@0.13.0`.

## v4.2.0 (2023-12-11)

### Changed

- Updated to `lighthouse@11.4.0`.
- Updated to Chromium 120.

## v4.1.1 (2023-11-14)

### Fixed

- Updated to `wait-on@7.2.0`, resolving CVE-2023-45857.

## v4.1.0 (2023-11-12)

### Changed

- Updated to Chromium 119.

## v4.0.0 (2023-11-03)

### Changed

- BREAKING: Updated image to Node 20, which is the active LTS release as of
  2023-10-24. (#10)
- Updated to `lighthouse@11.3.0` and `wait-on@7.1.0`.

## v3.2.0 (2023-10-12)

### Changed

- Updated to Chromium 118.

### Fixed

- Updated to `lighthouse@11.2.0`

## v3.1.0 (2023-09-19)

### Changed

- Updated to Chromium 117.

## v3.0.2 (2023-09-12)

### Fixed

- Updated to `lighthouse@11.1.0`

## v3.0.1 (2023-08-21)

### Fixed

- Updated to `serve@14.2.1`

## v3.0.0 (2023-08-20)

### Changed

- BREAKING: Updated to `lighthouse@11.0.0`. See the
  [breaking changes](https://github.com/GoogleChrome/lighthouse/releases/tag/v11.0.0)
  in v11.0.0.
- Updated to Chromium 116.

## v2.1.1 (2023-07-15)

### Fixed

- Updated to `lighthouse@10.4.0`.
- Updated transitive dependencies to resolve vulnerabilities
  [CVE-2023-37466](https://nvd.nist.gov/vuln/detail/CVE-2023-37466),
  [GHSA-cchq-frgv-rjh5](https://osv.dev/vulnerability/GHSA-cchq-frgv-rjh5),
  and [GHSA-g644-9gfx-q4q4](https://osv.dev/vulnerability/GHSA-g644-9gfx-q4q4).

## v2.1.0 (2023-07-07)

### Changed

- Updated base image to Debian Bookworm. (#7)

## v2.0.6 (2023-06-15)

### Fixed

- Updated to `lighthouse@10.3.0`.

## v2.0.5 (2023-06-03)

### Fixed

- Updated to Chromium 114.

### Miscellaneous

- Updated pipeline `needs` to optimize execution.

## v2.0.4 (2023-05-04)

### Fixed

- Updated to `lighthouse@10.2.0`.
- Update to Chromium 113.

### Miscellaneous

- Updated pipeline `needs` to optimize execution. (#6)

## v2.0.3 (2023-04-26)

### Fixed

- Added `procps` package to image to resolve issues spawning child processes.
  (#5)

## v2.0.2 (2023-04-14)

### Fixed

- Updated to `lighthouse@10.1.1` and `@lhci/cli@0.12.0`.

## v2.0.1 (2023-04-12)

### Fixed

- Updated to `lighthouse@10.1.0` and `@lhci/cli@0.11.1`.
- Update to Chromium 112.

## v2.0.0 (2023-02-21)

### Changed

- BREAKING: Updated to `lighthouse@10.0.1`. See the
  [breaking changes](https://github.com/GoogleChrome/lighthouse/releases/tag/v10.0.0)
  in v10.0.0.
- BREAKING: Updated to `@lhci/cli@0.11.0`. See the
  [breaking changes](https://github.com/GoogleChrome/lighthouse-ci/releases/tag/v0.11.0)
  in v0.11.0. Note this still uses `lighthouse@9.6.8`.

## v1.2.0 (2023-02-12)

### Changed

- Update project to pin all dependency versions (including transitive npm
  dependencies). Update renovate config to manage all dependency updates. (#1)

### Miscellaneous

- Added `prettier` pre-commit hook and CI job to check formatting.

## v1.1.1 (2023-02-11)

### Fixed

- Pinned to `lighthouse@9.6.8` until v10 issues can be resolved. (#4)

## v1.1.0 (2023-01-16)

### Changed

- Added [Lighthouse CI](https://www.npmjs.com/package/@lhci/cli) to the
  image, which can be run instead of Lighthouse when required. The other
  dependencies all remain the same.

## v1.0.0 (2023-01-15)

Initial release
